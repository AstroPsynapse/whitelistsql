package org.astropsynapse.WhitelistSQL;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class WhitelistSQL extends JavaPlugin {
    public final FileConfiguration config;
    public final ConsoleCommandSender console;
    private final Logger log;
    private final String version;
    private final String name;
    private final PluginManager pm;
    // Database connection
    public Connection dbConnection;
    private String dbHost, dbName, dbUsername, dbPassword;
    private int dbPort;

    public WhitelistSQL() {
        config = this.getConfig();
        console = this.getServer().getConsoleSender();
        log = this.getServer().getLogger();
        version = this.getDescription().getVersion();
        name = this.getDescription().getName();
        pm = this.getServer().getPluginManager();
    }

    @Override
    public void onEnable() {
        loadConfig();
        console.sendMessage(String.format("%s%s %s%s is now enabled.", ChatColor.LIGHT_PURPLE, name, version, ChatColor.RESET));

    }

    @Override
    public void onDisable() {
        console.sendMessage(String.format("%s%s %s%s is now disabled.", ChatColor.LIGHT_PURPLE, name, version, ChatColor.RESET));
    }

    private void loadConfig() {
        config.addDefault("host", "localhost");
        config.addDefault("port", 3306);
        config.addDefault("database", "whitelistsql");
        config.addDefault("username", "root");
        config.addDefault("password", "secret");
        config.options().copyDefaults(true);
        this.saveConfig();
        db_init();
    }

    private void db_init() {
        dbHost = config.getString("host");
        dbPort = config.getInt("port");
        dbName = config.getString("database");
        dbUsername = config.getString("username");
        dbPassword = config.getString("password");
        try {
            openConnection();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void openConnection() throws SQLException, ClassNotFoundException {
        if (dbConnection != null && !dbConnection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (dbConnection != null && !dbConnection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            dbConnection = DriverManager.getConnection("jdbc:mysql://" + this.dbHost+ ":" + this.dbPort + "/" + this.dbName, this.dbUsername, this.dbPassword);
        }
    }
}
